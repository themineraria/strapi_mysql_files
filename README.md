# strapi-mysql-files
Allows file hosting on a Mysql database.

Based on strapi-plugin-mongodb-files
see: https://github.com/AHgPuK/strapi-mongodb-files

The package contains two modules working separately.
The provider's one is attended for upload handling within an admin section.
The plugin's one provides an access to the previously uploaded files by URL.

## Installation

```   
npm i strapi-provider-upload-mysql strapi-plugin-mysql-files
```

## Configure

The doc source: https://strapi.io/documentation/v3.x/plugins/upload.html#using-a-provider

## Settings

```
You can configure a desired table for uploaded files instead default which is provider-upload-mysql
Edit strapi/config/custom.json
```

**Notes**
```
1. Uploaded files are case-sensitive
2. Changing the table name after file uploads 
   will result with an inaccessibility of previously uploaded files at "Files Upload" of strapi.
To fix it you need to replace the old path to the new one in "upload_file" collection of strapi.
```
